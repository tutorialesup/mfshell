import { loadRemoteModule } from '@angular-architects/module-federation';
import { Routes } from '@angular/router';

export const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.routes').then((m) => m.routes),
  },
  {
    path: 'comunidad',
    loadComponent: async () => await loadRemoteModule({
      type: 'module',
      remoteEntry: 'http://localhost:8102/remoteEntry.js',
      exposedModule: './Component'
    }).then(m => m.AppComponent)
  },
  {
    path: 'viajes',
    loadComponent: async () => await loadRemoteModule({
      type: 'module',
      remoteEntry: 'http://localhost:8102/remoteEntry.js',
      exposedModule: './Viajes'
    }).then(m => m.TravelPage)
  },
  {
    path: 'seguros',
    loadComponent: async () => await loadRemoteModule({
      type: 'module',
      remoteEntry: 'http://localhost:8102/remoteEntry.js',
      exposedModule: './Seguros'
    }).then(m => m.InsurancePage)
  },
  {
    path: 'descuentos',
    loadComponent: async () => await loadRemoteModule({
      type: 'module',
      remoteEntry: 'http://localhost:8102/remoteEntry.js',
      exposedModule: './Descuentos'
    }).then(m => m.DiscountsPage)
  },
  {
    path: 'estudios',
    loadComponent: async () => await loadRemoteModule({
      type: 'module',
      remoteEntry: 'http://localhost:8102/remoteEntry.js',
      exposedModule: './Estudios'
    }).then(m => m.StudiesPage)
  }
];
